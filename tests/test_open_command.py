"Open Command Tests"

from argparse import ArgumentParser
from unittest import TestCase
import re

from nose.plugins.attrib import attr
from nose.tools import assert_not_equal, eq_
from prettytable import PrettyTable

from rogue.core.open_command import OpenCommand
from rogue.core.proto import Proto

class TestOpenCommand(TestCase):
    @staticmethod
    def get_cell(table, name, field):
        for row in table:
            row_name = row.get_string(fields=["Name"], border=False, header=False).strip()

            if row_name == name:
                return row.get_string(fields=[field], border=False, header=False).strip()

    @staticmethod
    def get_sample_data():
        return {
            "apple": {
                "name": "Apple"
            },
            "ban": {
                "proto": "banana"
            },
            "banana": {
                "name": "Banana"
            },
            "echo": {
                "path": "echo",
                "shell": True
            },
            "echo_popen": {
                "proto": "echo",
                "popen": True
            },
            "exit": {
                "path": "exit",
                "shell": True
            },
            "exit_popen": {
                "proto": "exit",
                "popen": True
            },
            "mo_peach": {
                "proto": "missouri_peach",
                "path": "echo",
                "shell": True
            },
            "peach": {
                "name": "Peach",
                "arguments": "test"
            },
            "missouri_peach": {
                "proto": "peach",
                "name": "Missouri Peach"
            }
        }

    @staticmethod
    def get_args(args):
        parser = ArgumentParser(add_help=False)

        OpenCommand.set_parser(parser)

        return parser.parse_known_args(args)

    @staticmethod
    @attr("OpenCommand", "command")
    def test_cli_help():
        args = TestOpenCommand.get_args(["-h"])

        response = OpenCommand.command(*args, programs=TestOpenCommand.get_sample_data())

        eq_(response.get("command"), "help")
        assert isinstance(response.get("output"), PrettyTable)

    @staticmethod
    @attr("OpenCommand", "command")
    def test_cli_open_arguments():
        phrase = "it works"

        args = TestOpenCommand.get_args(["echo", phrase, "-s"])

        response = OpenCommand.command(*args, programs=TestOpenCommand.get_sample_data())
        process = response.get("output")
        error_code = process.wait()

        eq_(response.get("command"), "open")
        eq_(error_code, 0)

    @staticmethod
    @attr("OpenCommand", "get_help_table")
    def test_arguments():
        sample = TestOpenCommand.get_sample_data()
        table = OpenCommand._get_help_table(sample)

        name = "Missouri Peach"
        expected_output = "test"

        output = TestOpenCommand.get_cell(table, name, "Arguments")

        eq_(output, expected_output)

    @staticmethod
    @attr("OpenCommand", "get_help_table")
    def test_commands_sorted():
        sample = TestOpenCommand.get_sample_data()
        table = OpenCommand._get_help_table(sample)

        name = "Banana"
        commands = []

        for key in sample:
            item = Proto.get_expanded(sample, key)

            if item.get("name") == name:
                commands.append(key)

        commands.sort()
        expected_output = "\n".join(commands)

        output = TestOpenCommand.get_cell(table, name, "Commands")
        output = re.sub(r"[ \n]{2,}", "\n", output)
        output = output.strip()

        eq_(output, expected_output)

    @staticmethod
    @attr("OpenCommand", "open_program")
    def test_open_error():
        sample = TestOpenCommand.get_sample_data()

        process = OpenCommand._open_program("```this does not exist```", shell=True, programs=sample)
        error_code = process.wait()

        assert_not_equal(error_code, 0)

    @staticmethod
    @attr("OpenCommand", "open_program")
    def test_open_success():
        sample = TestOpenCommand.get_sample_data()

        process = OpenCommand._open_program("echo", "it popen'd", programs=sample)
        error_code = process.wait()

        eq_(error_code, 0)

    @staticmethod
    @attr("OpenCommand", "open_program")
    def test_open_wait_error():
        sample = TestOpenCommand.get_sample_data()

        error_code = OpenCommand._open_program("```this does not exist```", shell=True, wait=True, programs=sample)

        assert_not_equal(error_code, 0)

    @staticmethod
    @attr("OpenCommand", "open_program")
    def test_open_wait_success():
        sample = TestOpenCommand.get_sample_data()

        error_code = OpenCommand._open_program("echo_popen", ["it popen'd"], wait=True, programs=sample)

        eq_(error_code, 0)
