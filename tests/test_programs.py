"Test Programs"

from unittest import TestCase

from nose.tools import eq_
from nose.plugins.attrib import attr

from rogue.core.programs import Programs

class TestPrograms(TestCase):
    @staticmethod
    @attr("Programs", "get_programs")
    def test_file_does_not_exist():
        filename = "does not exist.nope"
        response = Programs.get_programs(filename)

        eq_(response, None)

    @staticmethod
    @attr("Programs", "get_programs")
    def test_file_exists():
        filename = "_programs_example.json"
        response = Programs.get_programs(filename)

        assert response.get("chrome")

    @staticmethod
    @attr("Programs", "get_expanded_program")
    def test_get_expanded_success():
        filename = "_programs_example.json"
        response = Programs.get_expanded_program("chrome", filename)

        assert response

    @staticmethod
    @attr("Programs", "get_expanded_program")
    def test_no_key():
        response = Programs.get_expanded_program(None)
        eq_(response, None)
