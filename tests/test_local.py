"Test Local Module"

from unittest import TestCase

from nose.tools import eq_

from rogue.core.local import Local

class TestLocal(TestCase):
    @staticmethod
    def test_file_does_not_exist():
        filename = "does not exist.nope"
        response = Local.get_json(filename)

        eq_(response, None)

    @staticmethod
    def test_file_success():
        filename = "_local_example.json"
        response = Local.get_json(filename)

        eq_(response.get("exists"), True)
