"My Process Tests"

from unittest import TestCase

from nose.tools import assert_not_equal, eq_

from rogue.core.my_process import call_and_log, popen_and_log

class TestMyProcess(TestCase):
    @staticmethod
    def test_error_code_error():
        error_code = call_and_log(["```this does not exist```"], shell=True)

        assert_not_equal(error_code, 0)

    @staticmethod
    def test_error_code_success():
        error_code = call_and_log(["echo", "test"], shell=True)

        eq_(error_code, 0)

    @staticmethod
    def test_popen_error_code_error():
        process = popen_and_log(["```this does not exist```"], shell=True)
        error_code = process.wait()

        assert_not_equal(error_code, 0)

    @staticmethod
    def test_popen_error_code_success():
        process = popen_and_log(["echo", "test"], shell=True)
        error_code = process.wait()

        eq_(error_code, 0)
