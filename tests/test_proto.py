"Proto Tests"

from unittest import TestCase

from nose.tools import eq_

from rogue.core.proto import Proto

class TestProto(TestCase):
    @staticmethod
    def get_sample_data():
        return {
            "apple": {
                "cost": "2.00",
                "type": "fruit"
            },
            "pear": {
                "proto": "apple",
                "cost": "1.00"
            },
            "spanish_pear": {
                "proto": "pear",
                "location": "spain"
            },
            "turkish_pear": {
                "proto": "spanish_pear",
                "location": "turkey"
            },
            "wrench": {
                "proto": "not_found",
                "cost": "10.00"
            }
        }

    @staticmethod
    def test_has_proto_attribute():
        pear = Proto.get_expanded(TestProto.get_sample_data(), "pear")

        assert pear
        eq_(pear.get("type"), "fruit")

    @staticmethod
    def test_no_proto():
        apple = Proto.get_expanded(TestProto.get_sample_data(), "apple")

        assert apple
        eq_(apple.get("type"), "fruit")

    @staticmethod
    def test_overwrite_proto_attribute():
        pear = Proto.get_expanded(TestProto.get_sample_data(), "pear")

        assert pear
        eq_(pear.get("cost"), "1.00")

    @staticmethod
    def test_preserves_proto_attribute():
        data = TestProto.get_sample_data()

        Proto.get_expanded(data, "spanish_pear")
        turkish_pear = Proto.get_expanded(data, "turkish_pear")

        assert turkish_pear
        eq_(turkish_pear.get("type"), "fruit")

    @staticmethod
    def test_proto_not_found():
        wrench = Proto.get_expanded(TestProto.get_sample_data(), "wrench")

        assert wrench
        eq_(wrench.get("cost"), "10.00")
