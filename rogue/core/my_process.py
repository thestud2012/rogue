"My Process"

import subprocess

from past.builtins import basestring

from rogue.core.cprint import CPrint

def call_and_log(*pargs, **kargs):
    """
    Log before and after subprocess.call

    Returns:

        int: The process exit code.
    """

    return _process_and_log(*pargs, process_function=subprocess.call, status_based_finish=True, **kargs)

def popen_and_log(*pargs, **kargs):
    """
    Logs before and after subprocess.popen

    Returns:

        Popen: The process.
    """

    return _process_and_log(*pargs, process_function=subprocess.Popen, **kargs)

def _process_and_log(*pargs, **kargs):
    args = pargs[0]
    process_function = kargs.pop("process_function")
    status_based_finish = kargs.pop("status_based_finish", None)

    if isinstance(args, basestring):
        simplified = args
    else:
        simplified = subprocess.list2cmdline(args)

    CPrint.warning("Starting: {}".format(simplified))

    response = process_function(*pargs, **kargs)

    finish_func = CPrint.warning

    if status_based_finish:
        finish_func = CPrint.success if response == 0 else CPrint.error

    finish_func("Finished: {}".format(simplified))

    return response
