"Programs"

from rogue.core.local import Local
from rogue.core.proto import Proto

class Programs(object):
    @staticmethod
    def get_programs(filename=None):
        filename = filename or "programs.json"
        return Local.get_json(filename)

    @staticmethod
    def get_expanded_program(key, filename=None, proto_phrase=None, programs=None):
        if not key:
            return None

        programs = programs or Programs.get_programs(filename)

        return Proto.get_expanded(programs, key, proto_phrase=proto_phrase)
