"Local Module"

import json
from os import path

class Local(object):
    @staticmethod
    def get_json(filename):
        """
        Gets json from filename.

        Note:
            Include extension.

        Args:

            filename (str): Filename in ../local/ path.

        :rtype: dict
        """

        current_path = path.dirname(__file__)
        file_path = path.join(current_path, "..", "..", "local", filename)

        if not path.isfile(file_path):
            return None

        with open(file_path) as pointer:
            return json.load(pointer)
