"Prototype"

class Proto(object):
    @staticmethod
    def get_expanded(dictionary, key, **kargs):
        """
        Get expanded item by key.

        Args:

            dictionary (dict): The source dictionary.
            key (str): The top-level key. *Not necessarily the top-most level*.
            proto_phrase (str, optional): Use phrase instead of default "proto".

        Returns:

            any: The expanded item.
        """

        proto_phrase = kargs.pop("proto_phrase", None) or "proto"

        current = dictionary.get(key)

        if not current:
            return None

        proto_key = current.get(proto_phrase)

        if not proto_key:
            return current

        proto = Proto.get_expanded(dictionary, proto_key, proto_phrase=proto_phrase)

        if not proto:
            return current

        # Preserve proto key
        proto_key = proto.get(proto_phrase)

        proto.update(current)

        proto[proto_phrase] = proto_key

        return proto
