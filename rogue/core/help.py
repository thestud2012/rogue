"Help Module"

from collections import defaultdict

from prettytable import PrettyTable

from rogue.core.local import Local
from rogue.core.proto import Proto

class Help(object):
    @staticmethod
    def get_help_table(**kargs):
        """
        Get help table from *json*.

        Note:
            Must pass in either *by_name* or *json*.

        Args:

            by_name (dict, optional): Use transformed json.
            json (dict, optional): Use instead of reading from file.

        :rtype: PrettyTable
        """

        by_name = kargs.pop("by_name", None)
        json = kargs.pop("json", None)

        if not by_name and not json:
            raise KeyError("Must pass in either by_name or json.")

        by_name = by_name or Help._get_by_name(json)

        table = PrettyTable(["Name", "Commands", "Arguments"])
        table.align = "l"

        for name, commands in by_name.items():
            command_keys = list(commands.keys())
            command_keys.sort()

            item = commands[command_keys[0]]

            commands = "\n".join(command_keys)

            arguments = item.get("arguments") or ""

            table.add_row([name, commands, arguments])

        max_width = 80
        padding = 5

        currently_used = table.get_string(fields=["Name", "Commands"])
        split = currently_used.split()[0]

        arguments_width = max_width - len(split) - padding
        table.max_width["Arguments"] = arguments_width

        table.sortby = "Name"

        return table

    @staticmethod
    def get_help_commands(**kargs):
        """
        Get help commands from the Settings class.

        Args:

            file_name (str, optional): Use instead of settings.json.
            json (dict, optional): Use instead of reading from file.

        :rtype: list
        """

        file_name = kargs.pop("file_name", None) or "settings.json"
        json = kargs.pop("json", None) or Local.get_json(file_name) or {}

        return json.get("help_commands") or []

    @staticmethod
    def _get_by_name(json, **kargs):
        """
        Get transformed *json*.

        Args:

            json (dict): Original json in command format.
            proto_phrase (str, optional): Use phrase instead of default "proto".

        :rtype: dict
        """

        by_name = defaultdict(dict)

        for key in json:
            item = Proto.get_expanded(json, key, **kargs)
            name = item.get("name") or "None"
            by_name[name][key] = item

        return by_name
