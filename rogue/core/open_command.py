"Open Command"

from __future__ import print_function

from rogue.core.help import Help
from rogue.core.my_process import call_and_log, popen_and_log
from rogue.core.programs import Programs

class OpenCommand(object):
    @staticmethod
    def set_parser(parser):
        """
        Set the *parser*.

        Args:

            parser (ArgumentParser): CLI parser.

        :rtype: ArgumentParser
        """

        parser.add_argument("primary", nargs="?")
        parser.add_argument("-s", "--shell", action="store_true")
        parser.add_argument("-t", "--tag")
        parser.add_argument("-w", "--wait", action="store_true")

        return parser

    @staticmethod
    def command(*pargs, **kargs):
        """
        Run main command.

        Args:

            pargs[0] (Namespace): Known arguments from CLI.
            pargs[1] (list): Unknown arguments from CLI.
            programs (dict, optional): Use instead of programs.json.

        Returns:

            dict: command, output
        """

        args = pargs[0]
        uargs = len(pargs) >= 2 and pargs[1]

        primary = args.primary and args.primary.lower()
        print_help = not primary or primary in Help.get_help_commands()
        shell = args.shell
        wait = args.wait

        programs = kargs.get("programs")

        if print_help:
            table = OpenCommand._get_help_table(programs)
            print(table)

            return {
                "command": "help",
                "output": table
            }

        return {
            "command": "open",
            "output": OpenCommand._open_program(primary, extra_args=uargs, shell=shell, wait=wait, programs=programs)
        }

    @staticmethod
    def _get_help_table(json=None):
        json = json or Programs.get_programs()

        return Help.get_help_table(json=json)

    @staticmethod
    def _open_program(primary, extra_args=None, shell=None, wait=None, programs=None):
        program = Programs.get_expanded_program(primary, programs=programs)

        if not program:
            program = {
                "path": primary
            }

        if not shell is None:
            program["shell"] = shell

        call_path = program.get("path") or primary

        call_args = [call_path]

        shell = program.get("shell")

        if extra_args:
            for arg in extra_args:
                call_args.append(arg)

        if wait:
            return call_and_log(call_args, shell=shell)
        else:
            return popen_and_log(call_args, shell=shell)
