from __future__ import print_function

from colorama import Back, Fore
import colorama

class CPrint(object):
    @staticmethod
    def cprint(*pargs, **kargs):
        """
        Print in color.

        Note:
            Standard format is 1 blank color character, 1 space, then content.

        Args:

            *pargs (tuple): Content.
            back (AnsiBack, optional): Background color.
            fore (AnsiFore, optional): Foreground color.
            body (bool, optional): Color the content passed in.
            left_border (bool, optional): Color the left border.
                Alias: lb
                Default: True
            right_border (bool, optional): Color the right border.
                Alias: rb
        """

        body = kargs.pop("body", None)
        left_border = kargs.pop("lb", None) or kargs.pop("left_border", None)
        right_border = kargs.pop("rb", None) or kargs.pop("right_border", None)

        if left_border is None:
            left_border = True

        back = kargs.pop("back", None)
        fore = kargs.pop("fore", None)

        buffer = []

        if left_border:
            CPrint._pipe_color(buffer, fore, back)

        if body:
            CPrint._pipe_color(buffer, fore, back, pargs)
        else:
            buffer.append(*pargs)

        if right_border:
            CPrint._pipe_color(buffer, fore, back)

        colorama.init()
        print(*tuple(buffer), **kargs)
        colorama.deinit()

    @staticmethod
    def error(*pargs, **kargs):
        """
        Print error with red bar.

        Args:

            *pargs (tuple, optional): Content.
                Default: Error
            back (AnsiBack, optional): Background color.
                Default: Back.RED
        """

        pargs = pargs or ("Error",)

        kargs["back"] = kargs.get("back") or Back.RED

        CPrint.cprint(*pargs, **kargs)

    @staticmethod
    def gray(*pargs, **kargs):
        """
        Print content with gray bar.

        Args:

            *pargs (tuple): Content.
            back (AnsiBack, optional): Background color.
                Default: Back.LIGHTBLACK_EX
        """

        kargs["back"] = kargs.get("back") or Back.LIGHTBLACK_EX

        CPrint.cprint(*pargs, **kargs)

    @staticmethod
    def info(*pargs, **kargs):
        """
        Print info with magenta bar.

        Args:

            *pargs (tuple, optional): Content.
                Default: Info
            back (AnsiBack, optional): Background color.
                Default: Back.MAGENTA
        """

        pargs = pargs or ("Info",)

        kargs["back"] = kargs.get("back") or Back.MAGENTA

        CPrint.cprint(*pargs, **kargs)

    @staticmethod
    def success(*pargs, **kargs):
        """
        Print success with green bar.

        Args:

            *pargs (tuple, optional): Content.
                Default: Success
            back (AnsiBack, optional): Background color.
                Default: Back.GREEN
        """

        pargs = pargs or ("Success",)

        kargs["back"] = kargs.get("back") or Back.GREEN

        CPrint.cprint(*pargs, **kargs)

    @staticmethod
    def warning(*pargs, **kargs):
        """
        Print warning with yellow bar.

        Args:

            *pargs (tuple, optional): Content.
                Default: Warning
            back (AnsiBack, optional): Background color.
                Default: Back.YELLOW
        """

        pargs = pargs or ("Warning",)

        kargs["back"] = kargs.get("back") or Back.YELLOW

        CPrint.cprint(*pargs, **kargs)

    @staticmethod
    def _pipe_color(buffer, fore=None, back=None, text=None):
        if fore:
            buffer.append(fore)

        if back:
            buffer.append(back)

        if text:
            buffer.append(*text)

        if back:
            buffer.append(Back.RESET)

        if fore:
            buffer.append(Fore.RESET)
