"Batch Command"

from __future__ import print_function
from builtins import input

from string import Template

from rogue.core.cprint import CPrint
from rogue.core.help import Help
from rogue.core.local import Local
from rogue.core.my_process import call_and_log
from rogue.core.proto import Proto

class BatchCommand(object):
    @staticmethod
    def set_parser(parser):
        """
        Set the *parser*.

        Args:

            parser (ArgumentParser): CLI parser.

        :rtype: ArgumentParser
        """

        parser.add_argument("batch_name", nargs="?")

        return parser

    @staticmethod
    def command(*pargs, **kargs):
        """
        Run main command.

        Args:

            pargs[0] (Namespace): Known arguments from CLI.
            pargs[1] (list): Unknown arguments from CLI.
            json (dict, optional): Use instead of batches.json.

        Returns:

            dict: command, output
        """

        args = pargs[0]
        uargs = len(pargs) >= 2 and pargs[1]

        batch_name = args.batch_name and args.batch_name.lower()
        json = kargs.get("json") or Local.get_json("batches.json")

        if not batch_name or batch_name in Help.get_help_commands():
            table = BatchCommand._get_help_table(json=json)
            print(table)

            return {
                "command": "help",
                "output": table
            }

        variables = uargs and BatchCommand._parse_arguments(uargs) or {}

        batch = Proto.get_expanded(json, batch_name)
        steps = batch.get("steps")

        if isinstance(steps, str):
            steps = [steps]

        total = len(steps)

        for counter, step in enumerate(steps):
            BatchCommand._process_step(step, variables, counter, total)

        return {
            "command": "run"
        }

    @staticmethod
    def _get_help_table(json=None):
        """
        Get help table from json.

        Args:

            json (dict): Use instead of reading from file.

        :rtype: PrettyTable
        """

        json = json or Local.get_json("batches.json")

        return Help.get_help_table(json=json)

    @staticmethod
    def _parse_arguments(args):
        """
        Parse list-based arguments into dictionary.

        Args:

            args (list): Arguments to be parsed.

        :rtype: dict
        """

        variables = {}
        last_key = None
        parg_counter = 0

        for item in args:
            if item[0] == "-":
                last_key = item[1:]
                variables[last_key] = True
            elif item[0:1] == "--":
                last_key = item[2:]
                variables[last_key] = True
            elif last_key:
                variables[last_key] = item
                last_key = None
            else:
                variables["pargs{}".format(parg_counter)] = item
                parg_counter += 1

        return variables

    @staticmethod
    def _process_step(step, variables, counter, total):
        """
        Process step.

        Args:

            step (dict): Current step.
            variables (dict): Stored variables.
            counter (int): Current iteration.
            total (int): Expected total iterations.
        """

        if isinstance(step, str):
            step = {
                "type": "cli",
                "format": step
            }

        step_name = step.get("name")
        step_type = step.get("type")
        _format = step.get("format")
        prompt = step.get("prompt")
        store = step.get("store")
        value = step.get("value")

        if step_name:
            title = "Step {}/{}: {}".format(counter + 1, total, step_name)
        else:
            title = "Step {}/{}".format(counter + 1, total)

        print()
        CPrint.gray(title, rb=True)

        cond = step.get("if")

        if not cond is None:
            formatted = Template(cond).safe_substitute(**variables)

            if formatted == cond or not eval(formatted): # pylint: disable=eval-used
                CPrint.info("Skipped")
                return

        if not _format is None:
            _value = Template(_format).safe_substitute(**variables)

            if _value != _format:
                value = _value

        if step_type == "alias":
            BatchCommand._handle_alias(variables, store, value)
        elif step_type == "cli":
            BatchCommand._handle_cli(value)
        elif step_type == "default":
            BatchCommand._handle_default(step, variables)
        elif step_type == "ensure":
            BatchCommand._handle_ensure(variables, prompt, store)
        elif step_type == "set":
            BatchCommand._handle_set(variables, store, value)

    @staticmethod
    def _handle_alias(variables, store, value):
        if value is None:
            CPrint.info("Skipped")
        else:
            variables[store] = value
            CPrint.info("Found")

    @staticmethod
    def _handle_cli(value):
        call_and_log(value, shell=True)

    @staticmethod
    def _handle_default(step, variables):
        store = step["store"]
        value = step["value"]

        if store in variables:
            CPrint.info("Not needed")
        else:
            CPrint.success("{} = {}".format(store, value))
            variables[store] = value

    @staticmethod
    def _handle_ensure(variables, prompt, store):
        if store in variables:
            CPrint.info("Found")
        else:
            prompt = prompt or "{}? ".format(store)
            CPrint.warning(prompt, end="")
            variables[store] = input()

    @staticmethod
    def _handle_set(variables, store, value):
        variables[store] = value
        CPrint.success("Success")
