@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET "currentPath=%~dp0"

CALL "!currentPath!ensure_path.bat"

IF %errorlevel% == 0 (
    CALL pip install -r "!currentPath!pip.txt"

    IF NOT %errorlevel% == 0 (
        PAUSE
    )
)
