@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET "currentPath=%~dp0"

powershell -ExecutionPolicy ByPass -file "!currentPath!../cmd/__ep__.ps1"

IF NOT %errorlevel% == 0 (
    PAUSE
)
