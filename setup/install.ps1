try {
    $current_directory = Split-Path $MyInvocation.MyCommand.Definition -Parent
    $install_bat = $current_directory + "/install_core.bat"
    $arguments = "& '" + $install_bat + "'"

    If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
        Start-Process powershell -Verb runAs -ArgumentList $arguments
    }
    else {
        Start-Process $arguments    
    }
}
catch {
    Write-Error $_.Exception.ToString()
    Read-Host -Prompt "Press Enter to continue"
}