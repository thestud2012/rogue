@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET flag=%1

IF /I "!flag!"=="M" (
	SET flag=Main
)

IF /I "!flag!"=="NH" (
	SET flag=NewHire
)

IF /I NOT "!flag!"=="A" ^
IF /I NOT "!flag!"=="B" ^
IF /I NOT "!flag!"=="C" ^
IF /I NOT "!flag!"=="Main" ^
IF /I NOT "!flag!"=="NewHire" (
	EXIT /B
)

ECHO !flag!