@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET program=%1
SET flag=%2
SET mode=%3

IF /I "!flag!"=="A" (
	SET "folder=Rogue Mercs (A)"
)

IF /I "!flag!"=="B" (
	SET "folder=Rogue Mercs (B)"
)

IF /I "!flag!"=="Main" (
	SET folder=Main
)

IF /I "!program!"=="Monaco" (
	SET source="C:\Repos\!folder!\Monaco\AppSpecific\App_App\x64\!mode!"
	SET dest="C:\Program Files\CMS\Monaco"
)

IF /I "!program!"=="RIDE" (
	SET source="C:\Repos\!folder!\Autotest\Source\MonacoTestLibrary\bin\!mode!"
	SET dest="C:\MonacoTestLibrary_AnyCpu"
)

IF NOT DEFINED source (SET fail=T)
IF NOT DEFINED dest (SET fail=T)

IF DEFINED fail (
	ECHO Unknown program: !program!
	ECHO VARS: Command: !command!; Program: !program!; Mode: !mode!
	ECHO ACTL: Copy
	EXIT /B
)

ECHO !source! !dest!
XCOPY !source! !dest! /Y