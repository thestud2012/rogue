"Special"

import json
from . import config

class Special():
    "Handles programs from settings"
    @staticmethod
    def get_specials():
        "Gets the specials from settings"
        with open(config.SPECIAL_FILE) as data_file:
            return json.load(data_file)

    @staticmethod
    def get_canon_special(key):
        "Gets the canonical special"

        specials = Special.get_specials()

        special = specials.get(key)

        while special is not None and special.get("alias-for") is not None:
            special = specials.get(special["alias-for"])

        return special
