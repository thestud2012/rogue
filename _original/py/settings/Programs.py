"Programs"

import json
from . import config

class Programs():
    "Handles programs from settings"
    @staticmethod
    def get_programs():
        "Gets the programs from settings"
        with open(config.PROGRAMS_FILE) as data_file:
            return json.load(data_file)

    @staticmethod
    def get_program_options():
        "Get program options"
        return Programs.get_programs().keys()

    @staticmethod
    def get_canon_program(key):
        "Gets the canonical program from settings"

        programs = Programs.get_programs()

        program = programs.get(key)

        while program is not None and program.get("alias-for") is not None:
            program = programs.get(program["alias-for"])

        return program
