"Configuration"
import os

_CUR = os.path.dirname(__file__)

PROGRAMS_FILE = _CUR + "/Programs.json"
SPECIAL_FILE = _CUR + "/Special.json"
