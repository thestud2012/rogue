"Main"

import argparse
from random import randint
from commands.CloseCommand import CloseCommand
from commands.OpenCommand import OpenCommand
from settings.Programs import Programs

QUOTES = [
    "Great kid. Don't get cocky.",
    "Laugh it up, fuzzball!",
    "I'm not sure what it is about them, but they're trouble.",
    "I'm only in it for the money.",
    "Never tell me the odds."
]

def close_init(args):
    "Close Init"
    CloseCommand(args.program).exec()


def open_init(args):
    "Open Init"
    OpenCommand(args.program, args.path).exec()

if __name__ == "__main__":
    QUOTE = QUOTES[randint(0, len(QUOTES) - 1)]

    PARSER = argparse.ArgumentParser(description="$Rogue: " + QUOTE)
    SUB = PARSER.add_subparsers()

    PROGRAM_OPTIONS = Programs.get_program_options()

    CLOSE = SUB.add_parser("close", aliases="x")
    CLOSE.add_argument("program", nargs="?", type=str.lower,
                       default="explorer", choices=PROGRAM_OPTIONS)
    CLOSE.set_defaults(func=close_init)

    OPEN = SUB.add_parser("open", aliases="o")
    OPEN.add_argument("program", nargs="?", type=str.lower,
                      default="explorer", choices=PROGRAM_OPTIONS)
    OPEN.add_argument("path", nargs="?", type=str.lower)
    OPEN.set_defaults(func=open_init)

    ARGS = PARSER.parse_args()
    ARGS.func(ARGS)
