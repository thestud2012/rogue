"Close Command"

import subprocess
from settings.Programs import Programs

class CloseCommand():
    "Close Command"

    def __init__(self, program=None):
        self.program = program or "explorer"

    def exec(self):
        "Execute"

        program = Programs.get_canon_program(self.program) or self.program

        process_name = program["process_name"]

        subprocess.call(["taskkill", "/f", "/im", process_name, "/t"], shell=True)
