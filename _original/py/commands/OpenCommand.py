"Open Command"

import os
import subprocess
from settings.Programs import Programs
from settings.Special import Special

class OpenCommand():
    "Open Command"

    def __init__(self, program=None, path=None):
        self.program = program or "explorer"
        self.path = path

    def exec(self):
        "Execute"

        program = Programs.get_canon_program(self.program) or self.program

        call_name = program["call_name"]
        shell = program.get("shell")

        args = [call_name]

        if self.path is not None:
            special = Special.get_canon_special(self.path)
            path = special["path"] if special is not None else self.path
            args.append(path)

        print("opening: ")
        print(args)

        if call_name == "start":
            os.system("start \"\" " + path)
            return

        subprocess.call(args, shell=shell)
