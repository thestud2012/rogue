@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET program=%1
SET programB=%2
SET flag=%3

IF /I "!program!"=="Chrome" (
	SET dest="C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
)

IF /I "!program!"=="GREP" (
	SET dest="C:\Program Files\grepWin\grepWin.exe"
)

IF /I "!program!"=="Explorer" (
	SET dest="C:\Windows\explorer.exe"
)

IF /I "!program!"=="Folder" (
	IF /I "!programB!"=="None" (
		SET root=C:\Repos
		SEt subFolder=
	)

	IF /I "!programB!"=="Monaco" (
		SET root=C:\Repos
		SET subFolder=Monaco
	)

	IF /I "!programB!"=="RIDE" (
		SET root=C:\Repos
		SET subFolder=Autotest
	)

	IF /I "!flag!"=="A" (
		SET "midFolder=Rogue Mercs (A)"
	)

	IF /I "!flag!"=="B" (
		SET "midFolder=Rogue Mercs (B)"
	)

	IF /I "!flag!"=="C" (
		SET "midFolder=Rogue Mercs (C)"
	)

	IF /I "!flag!"=="Main" (
		SET midFolder=Main
	)

	IF /I "!flag!"=="NewHire" (
		SET "root=S:\DrewRyder\NewHireInfo"
	)

	REM Construct path
	SET dest=!root!

	IF DEFINED midFolder (
		SET "dest=!dest!\!midFolder!"
	)

	IF DEFINED subFolder (
		SET "dest=!dest!\!subFolder!"
	)

	SET dest="!dest!"
)

IF /I "!program!"=="Monaco" (
	SET dest="C:\Program Files\CMS\Monaco\Monaco.exe"
)

IF /I "!program!"=="Outlook" (
	SET dest="C:\Program Files\Microsoft Office 15\root\office15\OUTLOOK.EXE"
)

IF /I "!program!"=="Pandora" (
	SET dest="C:\Program Files (x86)\Pandora\Pandora.exe"
)

IF /I "!program!"=="RepEdit" (
	SET dest="C:\Program Files (x86)\Nucletron\AutoTest\RepositoryEditor.exe"
)

IF /I "!program!"=="RIDE" (
	IF /I "!flag!"=="A" (
		SET "suite=Rogue Mercs (A)"
	)

	IF /I "!flag!"=="B" (
		SET "suite=Rogue Mercs (B)"
	)

	IF /I "!flag!"=="Main" (
		SET suite=Main
	)

	SET suitePath="C:\Repos\!suite!\Autotest\Tests"

	SET dest=pythonw "c:\python27\scripts\ride.py" --suite !suitePath!
)

IF /I "!program!"=="Skype" (
	SET dest="C:\Program Files\Microsoft Office 15\root\office15\lync.exe"
)

IF /I "!program!"=="Spy" (
	SET dest="C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\Tools\spyxx_amd64.exe"
)

IF /I "!program!"=="Sublime" (
	SET dest="C:\Program Files\Sublime Text 3\sublime_text.exe"
)

IF /I "!program!"=="VisualCode" (
	REM doesn't play well with START
	
	CALL Code -n
	EXIT /B
)

IF /I "!program!"=="VisualStudio" (
	SET dest="C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\devenv.exe"
)

IF NOT DEFINED dest (
	ECHO Unknown program: !program!
	ECHO VARS: Command: !command!; Program: !program!
	ECHO ACTL: Open
	EXIT /B
)

ECHO Opening !program! @ !dest!
START "" !dest!