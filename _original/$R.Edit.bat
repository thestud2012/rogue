@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET program=%1
SET flag=%2

IF /I "!flag!"=="A" (
	SET "folder=Rogue Mercs (A)"
)

IF /I "!flag!"=="B" (
	SET "folder=Rogue Mercs (B)"
)

IF /I "!flag!"=="C" (
	SET "folder=Rogue Mercs (C)"
)

IF /I "!flag!"=="Main" (
	SET "folder=Main"
)

IF /I "!program!"=="$R" (
	REM doesn't play well with START

	CALL Code -n "C:\Richmond Path\r"
	EXIT /B
)

IF /I "!program!"=="Monaco" (
	SET dest="C:\Repos\!folder!\Monaco\FocalSim.sln"
)

IF /I "!program!"=="RIDE" (
	SET dest="C:\Repos\!folder!\Autotest\Source\MonacoTestLibrary.sln"
)

IF NOT DEFINED dest (
	ECHO Unknown program !program!
	ECHO VARS: Command: !command!; Program: !program!; Flag: !flag!
	ECHO ACTL: Edit
	EXIT /B
)

ECHO Editing !program! @ !dest!
START "" !dest!