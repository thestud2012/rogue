@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET program=%1

IF /I "!program!"=="*" (
	SET program=Explorer
)

IF /I "!program!"=="$R" (
	SET program=$R
)

IF /I "!program!"=="F" (
	SET program=Folder
)

IF /I "!program!"=="IPY" (
	SET program=IronPython
)

IF /I "!program!"=="MON" (
	SET program=Monaco
)

IF /I "!program!"=="NUKE" (
	SET program=RepEdit
)

IF /I "!program!"=="OUT" (
	SET program=Outlook
)

IF /I "!program!"=="PAN" (
	SET program=Pandora
)

IF /I "!program!"=="SKY" (
	SET program=Skype
)

IF /I "!program!"=="SUB" (
	SET program=Sublime
)

IF /I "!program!"=="TALOS" (
	SET program=RIDE
)

IF /I "!program!"=="VC" (
	SET program=VisualCode
)

IF /I "!program!"=="VS" (
	SET program=VisualStudio
)

IF /I NOT "!program!"=="$R" ^
IF /I NOT "!program!"=="Explorer" ^
IF /I NOT "!program!"=="Folder" ^
IF /I NOT "!program!"=="IronPython" ^
IF /I NOT "!program!"=="Monaco" ^
IF /I NOT "!program!"=="RepEdit" ^
IF /I NOT "!program!"=="Outlook" ^
IF /I NOT "!program!"=="Pandora" ^
IF /I NOT "!program!"=="Skype" ^
IF /I NOT "!program!"=="Sublime" ^
IF /I NOT "!program!"=="RIDE" ^
IF /I NOT "!program!"=="VisualCode" ^
IF /I NOT "!program!"=="VisualStudio" (
	EXIT /B
)

ECHO !program!