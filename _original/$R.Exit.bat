@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET program=%1

IF /I "!program!"=="Chrome" (
	SET process=Chrome.exe
)

IF /I "!program!"=="GREP" (
	SET process=grepWin.exe
)

IF /I "!program!"=="Explorer" (
	SET process=explorer.exe
	
	TASKKILL /F /IM !process!
	START !process!
	EXIT /B
)

IF /I !program!=="IronPython" (
	SET process=ipy.exe
)

IF /I "!program!"=="Monaco" (
	SET process=Monaco.exe
)

IF /I "!program!"=="Outlook" (
	SET process=Outlook.exe
)

IF /I "!program!"=="Pandora" (
	SET process=Pandora.exe
)

IF /I "!program!"=="RepEdit" (
	SET process=RepositoryEditor.exe
)

IF /I "!program!"=="RIDE" (
	SET process=pythonw.exe
)

IF /I "!program!"=="Skype" (
	SET process=lync.exe
)

IF /I "!program!"=="Spy" (
	SET process=spyxx_amd64.exe
)

IF /I "!program!"=="Sublime" (
	SET process=sublime_text.exe
)

IF /I "!program!"=="TSM" (
	SET process=MonacoTsmComm.Utilities.ConsoleSimulatorHost.exe
)

IF /I "!program!"=="VisualCode" (
	SET process=Code.exe
)

IF /I "!program!"=="VisualStudio" (
	SET process=devenv.exe
)

IF NOT DEFINED process (
	ECHO Unknown program: !program!
	ECHO VARS: Command: !command!; Program: !program!
	ECHO ACTL: Exit
	EXIT /B
)

ECHO Exiting !program! @ !process!
TASKKILL /F /T /IM !process!