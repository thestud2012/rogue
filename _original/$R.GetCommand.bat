@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET command=%1

IF /I "!command!"=="~" (
	SET command=Edit
)

IF /I "!command!"=="C" (
	SET command=Copy
)

IF /I "!command!"=="O" (
	SET command=Open
)

IF /I "!command!"=="X" (
	SET command=Exit
)

IF /I NOT "!command!"=="Copy" ^
IF /I NOT "!command!"=="Edit" ^
IF /I NOT "!command!"=="Exit" ^
IF /I NOT "!command!"=="Open" (
	EXIT /B
)

ECHO !command!