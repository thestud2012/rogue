@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET "currentPath=%~dp0"

CALL python "!currentPath!py/main.py" %*
EXIT /B

REM $R {command:Open} {program:Folder} {programB:Monaco} {flag:A} {mode:Release}

REM $R Copy {program} {flag} {mode}
REM $R Edit {program} {flag}
REM $R Exit {program}
REM $R Open {program} {programB} {flag} {mode}

SET _output=$R_COMMAND.txt

SET command=%1
CALL $R.GetCommand.bat !command! > %_output%
SET /p _command=<%_output%

IF /I "!_command!"=="" (
	SET command=Open
	GOTO endCommand
)

SET command=!_command!
SHIFT

:endCommand

SET program=%1
CALL $R.GetProgram.bat !program! > %_output%
SET /p _program=<%_output%

IF /I "!_program!"=="" (
	IF /I NOT "!command!"=="Open" (
		ECHO Missing param: Program
		EXIT /B
	)

	IF /I "!command!"=="Open" (
		SET program=Folder
	)

	GOTO endProgram
)

SET program=!_program!
SHIFT

:endProgram

SET programB=%1
CALL $R.GetProgram.bat !programB! > %_output%
SET /p _programB=<%_output%

IF /I "!_programB!"=="" (
	SET programB=None
	GOTO endProgramB
)

SET programB=!_programB!
SHIFT

:endProgramB

SET flag=%1
CALL $R.GetFlag.bat !flag! > %_output%
SET /p _flag=<%_output%

IF /I "!_flag!"=="" (
	SET flag=A
	GOTO endFlag
)

SET flag=!_flag!
SHIFT

:endFlag

SET mode=%1
CALL $R.GetMode.bat !mode! > %_output%
SET /p _mode=<%_output%

IF /I "!_mode!"=="" (
	SET mode=Monaco
	GOTO endMode
)

SET mode=!_mode!
SHIFT

:endMode

IF /I "!command!"=="Copy" (
	CALL $R.Copy.bat !program! !flag! !mode!
	EXIT /B
)

IF /I "!command!"=="Edit" (
	CALL $R.Edit.bat !program! !flag!
	EXIT /B
)

IF /I "!command!"=="Exit" (
	CALL $R.Exit.bat !program!
	EXIT /B
)

IF /I "!command!"=="Open" (
	CALL $R.Open.bat !program! !programB! !flag!
	EXIT /B
)

ECHO Unknown Command: !command!