@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET mode=%1

IF /I "!mode!"=="D" (
	SET mode=Debug
)

IF /I "!mode!"=="R" (
	SET mode=Release
)

IF /I NOT "!mode!"=="Debug" ^
IF /I NOT "!mode!"=="Release" (
	EXIT /B
)

ECHO !mode!