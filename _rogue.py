"Main module"

from __future__ import print_function

from argparse import ArgumentParser
from string import Template
import sys

from rogue.core.batch_command import BatchCommand
# from app.build_command import BuildCommand
from rogue.core.help import Help
from rogue.core.open_command import OpenCommand
from rogue.core.proto import Proto

def get_commands():
    return {
        "ba": {
            "proto": "batch"
        },
        # "bu": {
        #     "proto": "build"
        # },
        "batch": {
            "name": "Batch",
            "arguments": "{name}",
            "command": BatchCommand()
        },
        # "build": {
        #     "name": "Build",
        #     "arguments": "{name} -[u]sing",
        #     "command": BuildCommand()
        # },
        "o": {
            "proto": "open"
        },
        "open": {
            "name": "Open",
            "arguments": "{name} -[s]hell -[t]ag -[w]ait",
            "command": OpenCommand(),
            "error_code": True
        }
    }

def main():
    parser = ArgumentParser(add_help=False)
    parser.add_argument("command", nargs="?")

    main_args = parser.parse_known_args()[0]
    arg_command = main_args.command

    commands = get_commands()
    help_commands = Help.get_help_commands()

    if arg_command and arg_command not in help_commands:
        command_args = sys.argv[2:]

        if not arg_command in commands:
            arg_command = "open"
            command_args = sys.argv[1:]

        item = Proto.get_expanded(commands, arg_command)
        parser = ArgumentParser(add_help=False)

        command = item["command"]
        command.set_parser(parser)

        args, uargs = parser.parse_known_args(args=command_args)
        response = command.command(args, uargs)

        if item.get("error_code"):
            error_code = response.get("output")

            if isinstance(error_code, int):
                sys.exit(error_code)

        return

    table = Help.get_help_table(json=commands)

    info_temp = """
Project: Rogue
Author:  Chris Richmond
Version: 0.1

Shortcut: ro {command} *args
Canonical: rogue {command} *args
Help: rogue $help_commands
    """

    help_commands = "[{}]".format(", ".join(help_commands))
    print(Template(info_temp).substitute(help_commands=help_commands))
    print(table)

if __name__ == "__main__":
    main()
