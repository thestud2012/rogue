@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET "currentPath=%~dp0"

powershell -ExecutionPolicy ByPass -file "!currentPath!setup/install.ps1"

IF %errorlevel% == 0 (
    ECHO Success
)

PAUSE
