try {
    $new_entry = Split-Path $MyInvocation.MyCommand.Definition -Parent
    $old_path = [Environment]::GetEnvironmentVariable('path', 'machine');

    if ($old_path.Contains($new_entry))
    {
        exit
    }

    $new_path = $old_path + ';' + $new_entry
    [Environment]::SetEnvironmentVariable('path', $new_path,'Machine');
}
catch {
    Write-Error $_.Exception.ToString()
    Read-Host -Prompt "Press Enter to continue"
}
