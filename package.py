import glob

from os import path

from zipfile import ZipFile
import zipfile

from past.builtins import basestring

def zip_it():
    current_path = path.dirname(__file__)
    zip_path = path.join(current_path, "Rogue.zip")

    project_files = {
        "": [
            "_rogue.py",
            "install.bat"
        ],
        "cmd": "*",
        "local": "*.json",
        "rogue": "*.py",
        "rogue/core": "*.py",
        "setup": "*"
    }

    with ZipFile(zip_path, "w", zipfile.ZIP_DEFLATED) as pointer:
        for key, value in project_files.items():
            child_path = key and path.join(current_path, key) or current_path

            if isinstance(value, basestring):
                value = [value]

            for item in value:
                for item_path in glob.glob(path.join(child_path, item)):
                    relative_path = path.relpath(item_path, current_path)
                    pointer.write(item_path, relative_path)

if __name__ == "__main__":
    zip_it()
