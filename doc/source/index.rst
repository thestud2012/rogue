.. Rogue documentation master file, created by
   sphinx-quickstart on Thu Mar 02 14:02:18 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Rogue's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: app

.. autoclass:: app.batch_command.BatchCommand
    :members:

.. autoclass:: app.cprint.CPrint
    :members:

.. autoclass:: app.help.Help
    :members:

.. autoclass:: app.local.Local
    :members:

.. automodule:: app.my_process
    :members:

.. autoclass:: app.open_command.OpenCommand
    :members:

.. autoclass:: app.proto.Proto
    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
